import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;


public class TennisTestCase {

	Tennis tenis;
	
	@Before
	public void setUp(){
		tenis = new Tennis();
	}

	
	@Test
	public void testInitialScore(){
		
		String result = "Love - Love";
		
		assertEquals(result, tenis.getScore()); 
	}

	@Test
	public void AddPointsToPlayerTest(){
		String result = "Fourty - Love";
		tenis.reset();
		tenis.wonPoint("player1");
		tenis.wonPoint("player1");
		tenis.wonPoint("player1");
		
		assertEquals(result, tenis.getScore());
		
		tenis.wonPoint("player2");
		
		result = "Fourty - Fifteen";
		assertEquals(result, tenis.getScore());
	}
	
	@Test
	public void getLastScore() {
		String result = "Deuce";
		tenis.reset();
		tenis.wonPoint("player1"); //15 -0
		tenis.wonPoint("player1"); // 30 - 0
		tenis.wonPoint("player1"); //40 - 0
		tenis.wonPoint("player2"); //40 - 15
		tenis.wonPoint("player2"); //40 - 30
		tenis.wonPoint("player2"); //40 - 40 -> Deuce
		
		assertEquals(result, tenis.getScore());
		
		result = "Advantage Player1";
		tenis.wonPoint("player1");
		assertEquals(result, tenis.getScore());
		
		/*tenis.wonPoint("player2");
		result = "Deuce";
		assertEquals(result, tenis.getScore());*/
	}
}
